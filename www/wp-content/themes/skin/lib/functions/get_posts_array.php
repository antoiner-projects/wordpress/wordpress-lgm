<?php

function get_posts_array($posts){
    $data = null;
    foreach($posts as $post_data){
        $postArray['title'] = $post_data->post_title;
        $postArray['author'] = get_the_author_meta('display_name', $post_data->post_author);
        $postArray['date'] = $post_data->post_date;
        $postArray['image'] = get_the_post_thumbnail_url($post_data->ID,'large');
        $postArray['url'] = get_post_permalink($post_data->ID);

        $main_categories = null;
        foreach(get_the_category($post_data->ID) as $category){
            if($category->category_parent !== 0){
                $postArray['sub_categories'][] = $category->name;
            }else{
                $main_categories[] = $category;
            }
        }
         if(isset($main_categories) && $main_categories[0] !== null){
             $postArray['main_category'] = $main_categories[0]->name;
             $category_data = get_term_meta($main_categories[0]->term_id);
         }else{
             $postArray['main_category'] = 'Hors Jeu';
         }

        $postArray['color'] = (isset($category_data['couleur'][0]) ? $category_data['couleur'][0] : "default");

        $data[] = $postArray;
        $postArray = null;
    }
    return $data;
}