<?php

function get_main_menu(){
    $menu = new TimberMenu('menu-principal');
    $items = $menu->items;
    foreach($items as &$item){

        $posts = null;
        if($item->object === "category"){
            $args = array(
                'posts_per_page' => '4',
                'cat' => $item->object_id
            );
            $data = new WP_Query( $args );

            foreach($data->posts as $post_data){

                $post['id'] = $post_data->ID;
                $post['title'] = $post_data->post_title;
                $post['author'] = get_the_author_meta('display_name', $post_data->post_author);
                $post['date'] = $post_data->post_date;
                $post['image'] = get_the_post_thumbnail_url($post_data->ID,'medium');
                $post['url'] = get_post_permalink($post_data->ID);

                $main_categories = null;
                foreach(get_the_category($post_data->ID) as $category){
                    if($category->category_parent !== 0){
                        $post['sub_categories'][] = $category->name;
                    }else{
                        $main_categories[] = $category->name;
                    }
                }
                $post['main_category'] = $main_categories[0];


                $posts[] = $post;
                $post = null;
            }
            $item->posts = $posts;
            $category_data = get_term_meta($item->object_id);
            $item->color = $category_data['couleur'][0];
        }
    }
    $menu->items = $items;
    return $menu;
}

