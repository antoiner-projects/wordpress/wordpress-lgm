<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */


require('lib/functions/get_posts_array.php');

$context = Timber::get_context();
$post = Timber::query_post();
$context['post'] = $post;

$compare_date = strtotime( "2018-08-09" );
$post_date    = strtotime( $post->post_date );

$context["large_banner"] = false;

if ( $compare_date < $post_date ) {
    $post_meta = get_post_meta($post->ID);
    $banner_type = $post_meta['banner_type'][0];
    if($banner_type == 'large'){
        $context["large_banner"] = true;
    }
}



$postData = null;
$main_categories = null;
foreach(get_the_category($post->ID) as $category){
    if($category->category_parent !== 0){
        $postData['sub_categories'][] = $category->name;
    }else{
        $main_categories[] = $category;
    }
}
$postData['main_category'] = $main_categories[0]->name;
$category_data = get_term_meta($main_categories[0]->term_id);


$postData['color'] = isset($category_data['couleur'][0]) ? $category_data['couleur'][0] : "default";
$context['post_data'] = $postData;

$similars = new WP_Query(
    array(
        'post_type'           => 'post',
        'posts_per_page'      => 3,
        'ignore_sticky_posts' => 1,
        'post__not_in'        => array($context['post']->ID),
        'cat'                 => $main_categories[0]->term_id
    )
);


$context['similars'] = get_posts_array($similars->posts);
$context['author'] = $post->author;


if ( post_password_required( $post->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	Timber::render( array( 'single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig' ), $context);
}
