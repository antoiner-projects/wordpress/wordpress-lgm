/* =============================================================================
   CONTROLLER: =Site
   ========================================================================== */

var site = (function(){
    var scrollTop = 0,
        previouScrollTop = 0;
    var documentWidth = null;
    var header;
    var scrollDirection = null;

    /**
     * Init
     */
    var init = function(){
        $ = jQuery;
        header = $("header.header");
        bindEvents();
        initSlideshow();
    };

    /**
     * Bind events
     */
    var bindEvents = function() {
        // Scroll handler
        $(window).on('scroll', scrollHandler);

        // Search toggle
        $('body').on('click', '.search-icon, .search-form-container .burger', searchToggle);
        $('.search-page').on('click', '#show-more-results', showMoreArticles);
    };

    /**
     * scrollHandler
     */
    var scrollHandler = function() {
        scrollTop = $(window).scrollTop();
        var previousScrollDirection = scrollDirection;
        scrollDirection = (scrollTop > previouScrollTop) ? 'down' : ((scrollTop === previouScrollTop) ? 'none' : 'up');

        header.removeClass("top-header");

        if (scrollDirection === "down") {
            header.removeClass("fixed")
                .addClass("hidden")
                .removeClass("header-top");
        }

        if (scrollDirection === "up" && previouScrollTop - scrollTop > 10) {
            header.addClass("fixed")
                .removeClass("hidden");
        }

        if(scrollDirection >= 0 && scrollTop < 50 || scrollTop <= 0 ){
            header.removeClass("hidden")
                .removeClass("fixed")
                .addClass("header-top");
        }

        $('.banner-large .article-banner').css({ 'opacity' : (1 - scrollTop/1000) });

        previouScrollTop = scrollTop;
    };

    /**
     * Toggle search
     */
    var searchToggle = function() {
        $('.search-form-container').toggleClass('active');
        $('body').toggleClass("search-active");
    };

    /**
     * Show more articles
     */
    var showMoreArticles = function() {
        $('.search-result-item').removeClass("hidden");
        $(this).addClass("hidden");
    };

    /**
     * Init Slideshow
     */
    var initSlideshow = function(){
        $('.slideshow').slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 576,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        removeSlide();
    };

    /**
     * remove Slide
     */
    var removeSlide = function(){
        documentWidth = $(document).width();
        if(documentWidth > 576){
            $('.slideshow.hide-first').slick('slickRemove', 0);
        }
    };

    /**
     * Public API
     */
    return {
        init: init
    }
})();

site.init();
