<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}


require('lib/functions/main_menu.php');

Timber::$dirname = array('templates', 'views');
add_theme_support( 'yoast-seo-breadcrumbs' );

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
        $this->register_shortcodes();
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

    function register_shortcodes( ) {
        require('lib/register_shortcodes.php');
    }




	function add_to_context( $context ) {
		$context['foo'] = 'bar';
		$context['stuff'] = 'I am a value set in your functions.php file';
		$context['notes'] = 'These values are available everytime you call Timber::get_context();';
		$context['main_menu'] = get_main_menu();
        $context['top_menu'] = new TimberMenu('top-menu');
        $context['footer_menu'] = new TimberMenu('menu-footer');
		$context['site'] = $this;
        if(function_exists('yoast_breadcrumb')){
            $context['breadcrumb'] = yoast_breadcrumb('<p>','</p>', false );
        }


		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new StarterSite();



function my_scripts(){
    wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/styles.min.css',false,'1.1','all');
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/assets/css/slick.css',false,'1.1','all');
    wp_enqueue_style( 'font-awesome.min', get_template_directory_uri() . '/assets/css/font-awesome.min.css',false,'1.1','all');
    wp_enqueue_script('slick-js', get_template_directory_uri() . '/assets/js/slick.min.js', array('jquery'), '1.0.0', true );
    wp_enqueue_script('modal-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap-modal.js', array('jquery'), '1.0.0', true );

    wp_enqueue_script('main-js', get_template_directory_uri() . '/assets/js/scripts.min.js', array('jquery'), '1.0.0', true );

}

add_action('wp_enqueue_scripts', 'my_scripts');

function wpc_show_admin_bar() {
    return false;
}
add_filter('show_admin_bar' , 'wpc_show_admin_bar');


function register_my_menus() {
    register_nav_menus(
        array(
            'top-menu' => __( 'Menu top' ),
            'main-menu' => __( 'Menu principal' ),
            'footer-menu' => __( 'Menu footer' )
        )
    );
}
add_action( 'init', 'register_my_menus' );
