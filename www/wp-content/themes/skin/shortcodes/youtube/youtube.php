<?php

add_shortcode('youtube', 'youtube_embed');

function youtube_embed($params){
    return convertYoutube($params[0]);
}

function convertYoutube($string) {
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<div class='iframe-container'><iframe src=\"//www.youtube.com/embed/$2\" frameborder='0' allowfullscreen></iframe></div>",
        $string
    );
}