<?php

function team_trombi( $atts ){

    $query = new WP_Query(array(
        'post_type' => 'member',
        'post_status' => 'publish',
        'posts_per_page' => -1
    ));

    $posts = null;
    foreach($query->posts as $post){
        $custom_fields = get_fields($post);
        if(isset($custom_fields['display']) && $custom_fields['display'] === 'true'){
            $data = array(
                'id'            => $post->ID,
                'name'          => $custom_fields['acf_name'] ? $custom_fields['acf_name'] : $post->title,
                'role'          => $custom_fields['acf_role'],
                'quote'         => $custom_fields['acf_quote'],
                'description'   => $custom_fields['acf_description'],
                'image'         => new TimberImage($custom_fields['acf_photo']['ID']),
                'social'        => $custom_fields['acf_social']
            );
            if($data['name'] !== ''){
                $posts[] = $data;
            }
            $data = null;
        }
    }


    $users = get_users( array('fields' => 'all') );

    foreach($users as $post){
        $custom_fields = get_fields($post);
        if(isset($custom_fields['display']) && $custom_fields['display'] === 'true'){
            $data = array(
                'id'            => $post->ID,
                'name'          => $custom_fields['acf_name'] ? $custom_fields['acf_name'] : $post->title,
                'role'          => $custom_fields['acf_role'],
                'quote'         => $custom_fields['acf_quote'],
                'description'   => $custom_fields['acf_description'],
                'image'         => new TimberImage($custom_fields['acf_photo']['ID']),
                'social'        => $custom_fields['acf_social']
            );
            if($data['name'] !== ''){
                $posts[] = $data;
            }
            $data = null;
        }

    }

    $context['members'] = $posts;

    return Timber::compile( 'team.twig', $context);
}




add_shortcode( 'team', 'team_trombi' );