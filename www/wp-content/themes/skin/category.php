<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

require('lib/functions/get_posts_array.php');
$context = Timber::get_context();

$context['term_page'] = new TimberTerm();
$posts = new Timber\PostQuery();
$context['posts'] = get_posts_array($posts);
$context['pagination'] = get_the_posts_pagination();


$templates = array( 'index.twig' );
if ( is_home() ) {
    array_unshift( $templates, 'index.twig' );
}
Timber::render( $templates, $context );
