<?php
/**
 * Search results page
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

require('lib/functions/get_posts_array.php');

$context = Timber::get_context();

$context['search_count'] = $wp_query->found_posts;
$context['search_key'] = get_search_query();


$args = array(
    'posts_per_page' => -1,
    'post_type' => 'post',
    's' => get_search_query()
);


$posts = Timber::get_posts($args);
$context['posts'] = get_posts_array($posts);

Timber::render('Templates/search.twig', $context);



