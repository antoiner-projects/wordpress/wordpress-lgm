<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

require('lib/functions/get_posts_array.php');
$context = Timber::get_context();
$stickies = get_option( 'sticky_posts' );

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$stickydata = null;
if(1 == $paged && $stickies) {
    $args = [
        'post_type'           => 'post',
        'post__in'            => $stickies,
        'posts_per_page'      => -1
    ];
    $the_query = new WP_Query($args);

    if ( $the_query->have_posts() ) {
        $stickydata = get_posts_array($the_query->posts);
    }
}

$args = array(
    'post_type'           => 'post',
    'ignore_sticky_posts' => 1,
    'post__not_in'        => $stickies
);
$args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

$custom_query = new WP_Query( $args );
$wp_query = new WP_Query();
$temp_query = $wp_query;
$wp_query   = NULL;
$wp_query   = $custom_query;

// Output custom query loop
$data = null;
if ( $custom_query->have_posts() ) :
    $data = get_posts_array($custom_query->posts);
endif;
$pagination = get_the_posts_pagination();
wp_reset_postdata();

$wp_query = NULL;
$wp_query = $temp_query;


$context['posts'] = $data;
$context['stickyposts'] = $stickydata;
$context['pagination'] = $pagination;

$context['foo'] = 'bar';
$templates = array( 'index.twig' );

if ( is_home() ) {
    array_unshift( $templates, 'index.twig' );
}
Timber::render( $templates, $context );
