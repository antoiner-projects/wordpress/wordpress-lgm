<?php

/**
 * Registers the `member` post type.
 */
function member_init() {
	register_post_type( 'member', array(
		'labels'                => array(
			'name'                  => __( 'Membres', 'custom-post-types' ),
			'singular_name'         => __( 'Membre', 'custom-post-types' ),
			'all_items'             => __( 'All Membres', 'custom-post-types' ),
			'archives'              => __( 'Membre Archives', 'custom-post-types' ),
			'attributes'            => __( 'Membre Attributes', 'custom-post-types' ),
			'insert_into_item'      => __( 'Insert into membre', 'custom-post-types' ),
			'uploaded_to_this_item' => __( 'Uploaded to this membre', 'custom-post-types' ),
			'featured_image'        => _x( 'Featured Image', 'member', 'custom-post-types' ),
			'set_featured_image'    => _x( 'Set featured image', 'member', 'custom-post-types' ),
			'remove_featured_image' => _x( 'Remove featured image', 'member', 'custom-post-types' ),
			'use_featured_image'    => _x( 'Use as featured image', 'member', 'custom-post-types' ),
			'filter_items_list'     => __( 'Filter membres list', 'custom-post-types' ),
			'items_list_navigation' => __( 'Membres list navigation', 'custom-post-types' ),
			'items_list'            => __( 'Membres list', 'custom-post-types' ),
			'new_item'              => __( 'New Membre', 'custom-post-types' ),
			'add_new'               => __( 'Add New', 'custom-post-types' ),
			'add_new_item'          => __( 'Add New Membre', 'custom-post-types' ),
			'edit_item'             => __( 'Edit Membre', 'custom-post-types' ),
			'view_item'             => __( 'View Membre', 'custom-post-types' ),
			'view_items'            => __( 'View Membres', 'custom-post-types' ),
			'search_items'          => __( 'Search membres', 'custom-post-types' ),
			'not_found'             => __( 'No membres found', 'custom-post-types' ),
			'not_found_in_trash'    => __( 'No membres found in trash', 'custom-post-types' ),
			'parent_item_colon'     => __( 'Parent Membre:', 'custom-post-types' ),
			'menu_name'             => __( 'Membres', 'custom-post-types' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'member',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'member_init' );

/**
 * Sets the post updated messages for the `member` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `member` post type.
 */
function member_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['member'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Membre updated. <a target="_blank" href="%s">View membre</a>', 'custom-post-types' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'custom-post-types' ),
		3  => __( 'Custom field deleted.', 'custom-post-types' ),
		4  => __( 'Membre updated.', 'custom-post-types' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Membre restored to revision from %s', 'custom-post-types' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Membre published. <a href="%s">View membre</a>', 'custom-post-types' ), esc_url( $permalink ) ),
		7  => __( 'Membre saved.', 'custom-post-types' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Membre submitted. <a target="_blank" href="%s">Preview membre</a>', 'custom-post-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Membre scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview membre</a>', 'custom-post-types' ),
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Membre draft updated. <a target="_blank" href="%s">Preview membre</a>', 'custom-post-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'member_updated_messages' );

