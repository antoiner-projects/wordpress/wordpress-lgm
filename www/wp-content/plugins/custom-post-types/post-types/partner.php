<?php

/**
 * Registers the `partner` post type.
 */
function partner_init() {
	register_post_type( 'partner', array(
		'labels'                => array(
			'name'                  => __( 'Partenaires', 'custom-post-types' ),
			'singular_name'         => __( 'Partenaire', 'custom-post-types' ),
			'all_items'             => __( 'All Partenaires', 'custom-post-types' ),
			'archives'              => __( 'Partenaire Archives', 'custom-post-types' ),
			'attributes'            => __( 'Partenaire Attributes', 'custom-post-types' ),
			'insert_into_item'      => __( 'Insert into partenaire', 'custom-post-types' ),
			'uploaded_to_this_item' => __( 'Uploaded to this partenaire', 'custom-post-types' ),
			'featured_image'        => _x( 'Featured Image', 'partner', 'custom-post-types' ),
			'set_featured_image'    => _x( 'Set featured image', 'partner', 'custom-post-types' ),
			'remove_featured_image' => _x( 'Remove featured image', 'partner', 'custom-post-types' ),
			'use_featured_image'    => _x( 'Use as featured image', 'partner', 'custom-post-types' ),
			'filter_items_list'     => __( 'Filter partenaires list', 'custom-post-types' ),
			'items_list_navigation' => __( 'Partenaires list navigation', 'custom-post-types' ),
			'items_list'            => __( 'Partenaires list', 'custom-post-types' ),
			'new_item'              => __( 'New Partenaire', 'custom-post-types' ),
			'add_new'               => __( 'Add New', 'custom-post-types' ),
			'add_new_item'          => __( 'Add New Partenaire', 'custom-post-types' ),
			'edit_item'             => __( 'Edit Partenaire', 'custom-post-types' ),
			'view_item'             => __( 'View Partenaire', 'custom-post-types' ),
			'view_items'            => __( 'View Partenaires', 'custom-post-types' ),
			'search_items'          => __( 'Search partenaires', 'custom-post-types' ),
			'not_found'             => __( 'No partenaires found', 'custom-post-types' ),
			'not_found_in_trash'    => __( 'No partenaires found in trash', 'custom-post-types' ),
			'parent_item_colon'     => __( 'Parent Partenaire:', 'custom-post-types' ),
			'menu_name'             => __( 'Partenaires', 'custom-post-types' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'partner',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'partner_init' );

/**
 * Sets the post updated messages for the `partner` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `partner` post type.
 */
function partner_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['partner'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Partenaire updated. <a target="_blank" href="%s">View partenaire</a>', 'custom-post-types' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'custom-post-types' ),
		3  => __( 'Custom field deleted.', 'custom-post-types' ),
		4  => __( 'Partenaire updated.', 'custom-post-types' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Partenaire restored to revision from %s', 'custom-post-types' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Partenaire published. <a href="%s">View partenaire</a>', 'custom-post-types' ), esc_url( $permalink ) ),
		7  => __( 'Partenaire saved.', 'custom-post-types' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Partenaire submitted. <a target="_blank" href="%s">Preview partenaire</a>', 'custom-post-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Partenaire scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview partenaire</a>', 'custom-post-types' ),
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Partenaire draft updated. <a target="_blank" href="%s">Preview partenaire</a>', 'custom-post-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'partner_updated_messages' );
