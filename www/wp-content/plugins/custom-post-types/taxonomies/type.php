<?php

/**
 * Registers the `type` taxonomy,
 * for use with 'partner'.
 */
function type_init() {
	register_taxonomy( 'type', array( 'partner' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_ui'           => true,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts',
		),
		'labels'            => array(
			'name'                       => __( 'Types', 'custom-post-types' ),
			'singular_name'              => _x( 'Type', 'taxonomy general name', 'custom-post-types' ),
			'search_items'               => __( 'Search Types', 'custom-post-types' ),
			'popular_items'              => __( 'Popular Types', 'custom-post-types' ),
			'all_items'                  => __( 'All Types', 'custom-post-types' ),
			'parent_item'                => __( 'Parent Type', 'custom-post-types' ),
			'parent_item_colon'          => __( 'Parent Type:', 'custom-post-types' ),
			'edit_item'                  => __( 'Edit Type', 'custom-post-types' ),
			'update_item'                => __( 'Update Type', 'custom-post-types' ),
			'view_item'                  => __( 'View Type', 'custom-post-types' ),
			'add_new_item'               => __( 'New Type', 'custom-post-types' ),
			'new_item_name'              => __( 'New Type', 'custom-post-types' ),
			'separate_items_with_commas' => __( 'Separate types with commas', 'custom-post-types' ),
			'add_or_remove_items'        => __( 'Add or remove types', 'custom-post-types' ),
			'choose_from_most_used'      => __( 'Choose from the most used types', 'custom-post-types' ),
			'not_found'                  => __( 'No types found.', 'custom-post-types' ),
			'no_terms'                   => __( 'No types', 'custom-post-types' ),
			'menu_name'                  => __( 'Types', 'custom-post-types' ),
			'items_list_navigation'      => __( 'Types list navigation', 'custom-post-types' ),
			'items_list'                 => __( 'Types list', 'custom-post-types' ),
			'most_used'                  => _x( 'Most Used', 'type', 'custom-post-types' ),
			'back_to_items'              => __( '&larr; Back to Types', 'custom-post-types' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'type',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'type_init' );

/**
 * Sets the post updated messages for the `type` taxonomy.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `type` taxonomy.
 */
function type_updated_messages( $messages ) {

	$messages['type'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => __( 'Type added.', 'custom-post-types' ),
		2 => __( 'Type deleted.', 'custom-post-types' ),
		3 => __( 'Type updated.', 'custom-post-types' ),
		4 => __( 'Type not added.', 'custom-post-types' ),
		5 => __( 'Type not updated.', 'custom-post-types' ),
		6 => __( 'Types deleted.', 'custom-post-types' ),
	);

	return $messages;
}
add_filter( 'term_updated_messages', 'type_updated_messages' );
