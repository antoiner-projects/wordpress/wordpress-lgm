<?php
/**
 * Plugin Name:     types de contenus personnalisés
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          arichard
 * Author URI:      YOUR SITE HERE
 * Text Domain:     custom-post-types
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Custom_Post_Types
 */

// Your code starts here.


require('post-types/member.php');
require('post-types/partner.php');


require('taxonomies/type.php');



function wpb_change_title_text( $title ){
    $screen = get_current_screen();

    if  ( 'member' == $screen->post_type ) {
        $title = 'Nom / Pseudo';
    }
    if  ( 'partner' == $screen->post_type ) {
        $title = 'Nom du partenaire';
    }

    return $title;
}

add_filter( 'enter_title_here', 'wpb_change_title_text' );

function wpb_change_editor_text( $desc ){
    $screen = get_current_screen();
    if  ( 'partner' == $screen->post_type ) {
        $desc = 'Description';
    }

    return $desc;

}

add_filter( 'write_your_story', 'wpb_change_editor_text');

